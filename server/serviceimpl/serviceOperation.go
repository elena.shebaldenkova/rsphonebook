package serviceimpl

import (
	pb "rsphonebook/server/servermodel"

	"golang.org/x/net/context"
	"rsphonebook/server/dao"
	"rsphonebook/server/utils"
	"log"
	"github.com/golang/protobuf/ptypes/empty"
	"time"

	)

type Server struct{
	// Returns the current time.
	Now func() time.Time
}

func (s *Server) FindRec(ctx context.Context, request *pb.RecordRequest) ( *pb.RecordResponse, error) {
	rec, err := dao.FindRec(request.GetPhonenumber())
	if err != nil {
		return &pb.RecordResponse{Status: pb.RecordResponse_BAD_REQUEST}, nil
	} else {
		return &pb.RecordResponse{Record: rec, Status: pb.RecordResponse_OK}, nil
	}

}



func (s *Server) DeleteRec(ctx context.Context, request *pb.RecordRequest) (*pb.RecordResponse, error) {
	err := dao.DeleteRec(request.GetPhonenumber())
	if err != nil {
		return &pb.RecordResponse{Status: pb.RecordResponse_BAD_REQUEST}, nil
	} else {
		return &pb.RecordResponse{Status: pb.RecordResponse_OK}, nil
	}

}

func (s *Server) UpdateRecord(ctx context.Context, request *pb.RecordRequest) (*pb.RecordResponse, error) {
	err := dao.UpdateRecord(request.GetRecord())

	if err != nil {
		return &pb.RecordResponse{Status: pb.RecordResponse_BAD_REQUEST}, nil
	} else {
		return &pb.RecordResponse{Record: request.GetRecord(), Status: pb.RecordResponse_UPDATED}, nil
	}

}


func (s *Server) ViewAll(request *empty.Empty, stream pb.PhoneBook_ViewAllServer) error {

	contactList, err := dao.ViewAll()
	if err != nil {
		log.Fatalf("dao.ViewAll() erro: %v", err)
	}
	for _, contact := range contactList {
		response := &pb.RecordResponse{Record: contact, Status: pb.RecordResponse_OK}
		if err := stream.Send(response); err != nil {
			return err
		}
	}

	return nil
}


func (s *Server) AddNewRecord(ctx context.Context, request *pb.RecordRequest) (*pb.RecordResponse, error) {
	if !utils.CheckValidNumber(request.GetRecord().Phone) {
		return &pb.RecordResponse{Status: pb.RecordResponse_BAD_REQUEST}, nil
	}
	err := dao.AddNewRecord(request.GetRecord())
	if err != nil {
		return &pb.RecordResponse{Status: pb.RecordResponse_SERVER_ERROR}, nil
	} else {
		return &pb.RecordResponse{Status: pb.RecordResponse_CREATED}, nil
	}

}
