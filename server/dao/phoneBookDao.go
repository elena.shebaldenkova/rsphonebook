package dao

import (
	"github.com/boltdb/bolt"
	"time"
	pbe "rsphonebook/client/clientmodel"
	"fmt"
	"github.com/golang/protobuf/proto"
	"errors"
)

var DB *bolt.DB = nil

const PATH_TO_DB = "C:/goPath/src/rsphonebook/my.db"
const BucketName = "PHONEBOOK"

func OpenConnectionToDB() {
	//bolt start
	DB, _ = bolt.Open(PATH_TO_DB, 0600, &bolt.Options{Timeout: 1 * time.Second})
}


func CloseConnectionToDB() {
	DB.Close()
}

func ViewAll() ([]*pbe.Record, error) {
	var contactList []*pbe.Record

	err := DB.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(BucketName))
		if bucket == nil {
			return errors.New("Bucket not exist, start from AddNewRecord")
		}
		bucket.ForEach(func(k, v []byte) error {
			contact := &pbe.Record{}
			if err := proto.Unmarshal(v, contact); err != nil {
				return err
			}
			contactList = append(contactList, contact)
			return nil
		})

		return nil
	})
	if err != nil {
		return nil, err
	}
	return contactList, nil
}


func AddNewRecord(record *pbe.Record) (er error) {
	er = DB.Update(func(tx *bolt.Tx) error {
		bucket, err := tx.CreateBucketIfNotExists([]byte(BucketName))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		if row := bucket.Get([]byte(record.Phone)); row != nil {
			return errors.New("Record is present in DB")
		}
		if buf, err := proto.Marshal(record); err != nil {
			return errors.New("Marshaling error")
		} else if err := bucket.Put([]byte(record.Phone), buf); err != nil {
			fmt.Println(err)
			return errors.New("Writing in base error")
		}
		return nil //You commit the transaction by returning nil at the end
	})
	if er != nil {
		return er
	}
	return nil

}

func FindRec (phonenumber string ) (*pbe.Record, error) {
record := &pbe.Record{}
	err := DB.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(BucketName))
		if bucket == nil {
			return errors.New("Bucket not exist, start from AddNewRecord")
		}

		buf := bucket.Get([]byte(phonenumber))
		if buf != nil {
			if err := proto.Unmarshal(buf, record); err != nil {
				return err
			}
		} else {
			return errors.New("Record not found")
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return record, err

}

func DeleteRec(phonenumber string) (err error) {

	err = DB.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(BucketName))
		if bucket == nil {
			return errors.New("Bucket not exist, start from AddNewRecord")
		}

		if row := bucket.Get([]byte(phonenumber)); row == nil {
			return errors.New("record is absent in DB")

		} else if err := bucket.Delete([]byte(phonenumber)); err != nil {
			return errors.New("error pending deleting")
		}

		return nil
	})
	if err != nil {
		return err
	}
	return  nil

}


func UpdateRecord(record *pbe.Record) (err error) {
	err = DB.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(BucketName))
		if bucket == nil {
			return errors.New("Bucket not exist, start from AddNewRecord")
		}

		if next := bucket.Get([]byte(record.Phone)); next != nil {
			if buf, err := proto.Marshal(record); err != nil {
				return err
			} else if err := bucket.Put([]byte(record.Phone), buf); err != nil {
				return err
			}
		} else if next == nil {
			return errors.New("record is absent in DB")
		}
		return err
	})
	if err != nil {
		return err
	}
	return nil

}
