// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	pb "rsphonebook/server/servermodel"
	"log"
	"net"
	so "rsphonebook/server/serviceimpl"

	"github.com/spf13/cobra"
	"google.golang.org/grpc"
	"rsphonebook/server/dao"
)

const (
	port = ":10000"
)

// startCmd represents the start command
var startCmd = &cobra.Command{
	Use:   "start",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command.`,

	Run: func(cmd *cobra.Command, args []string) {
		dao.OpenConnectionToDB()

		lis, err := net.Listen("tcp", port)
		if err != nil {
			log.Fatalf("failed to listen: %v", err)
		}
		log.Println("Start server")
		// Creates a new gRPC server
		s := grpc.NewServer()
		pb.RegisterPhoneBookServer(s, &so.Server{})
		if err := s.Serve(lis); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
		log.Println("Start server")

	},
}

func init() {
	rootCmd.AddCommand(startCmd)
}
