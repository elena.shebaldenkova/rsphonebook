***Проект "Телефонная книга"***

***Используемые технологии - Сobra, Protobuf, Bolt.db, Rest, Swagger***


СОЗДАНИЕ ПРОЕКТА
-  С помощью консоли и комманд Кобры создана нужная структура проекта
-  Создала 2 прото файла(Модель самой записи с необходимыми полями и Модель реквест/респонс и сервисы для сервера)
-  Генерация файлов на основе протофайлов(из директории  C:\GoPath\bin\protoc-3.5.0-win32\bin запустить такие комманды: 

//клиент
protoc -I=C:/GoPath/src C:/GoPath/src/rsphonebook/client/clientmodel/record.proto --go_out=plugins=grpc:C:/GoPath/src

//сервер
protoc -I=C:\GoPath\src -I=C:\GoPath\src\github.com\grpc-ecosystem\grpc-gateway\third_party\googleapis --grpc-gateway_out=logtostderr=true:C:\GoPath\src --go_out=plugins=grpc:C:\GoPath\src C:\GoPath\src\rsphonebook\server\servermodel\recordServer.proto

//swagger
protoc -I=C:\GoPath\src -I=C:\GoPath\src\github.com\grpc-ecosystem\grpc-gateway\third_party\googleapis --swagger_out=logtostderr=true:C:\GoPath\src --go_out=plugins=grpc:C:\GoPath\src C:\GoPath\src\rsphonebook\server\servermodel\recordServer.proto

-  Вынесен код для работы с бд в отдельный файл
-  Имплементация методов вынесена также в отдельный файл
-  Реализован старт grpc-servera
-  В папку ui вынесена работа со сваггером(старт вебсервера, статические файлы для работы сваггера, перенесен(и прописан путь к нему в файле index.html) сгенерированный джейсон, из консоли из директории
-  C:\GoPath\src\rsphonebook\ui\swaggerui командой go-bindata-assetfs -pkg=swaggerui static/... сгенерирован файл для работы сваггера)



***ЗАПУСК ПРОЕКТА***

Скачать архив по ссылке https://gitlab.com/elena.shebaldenkova/rsphonebook , распаковать его

Установить необходимые зависимости 

go get -u github.com/spf13/cobra/cobra

go get github.com/golang/protobuf/protoc-gen-go
 
go get github.com/boltdb/bolt/...

go get -u google.golang.org/grpc



По умолчанию в проекте установлены значения для констант(при пнеобходимости - изменить на свои значения):

"port" - IP адресс машины, на которой проводится запуск сервера и порт (server/cmd/start.go и ui/server/server.go)

"PATH_TO_DB" - полный путь к базе данных (в пакете server/dao/phoneBookDao.go)



После установки в консоли перейти в директорию проекта в папку

 /rsphonebook/server, ввести go run main.go start 
 
 /rsphonebook/ui, ввести go run main.go start

после переходим в браузер - ***http://localhost:8080/swaggerui/#*** и видим все реализованные методы.

