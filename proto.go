package main

//генерация прото клиента:
//перейти в директорию C:\GoPath\bin\protoc-3.5.0-win32\bin и запустить


//клиент
//go:generate protoc -I=C:/GoPath/src C:/GoPath/src/rsphonebook/client/clientmodel/record.proto --go_out=plugins=grpc:C:/GoPath/src

//сервер
//go:generate protoc -I=C:\GoPath\src -I=C:\GoPath\src\github.com\grpc-ecosystem\grpc-gateway\third_party\googleapis --grpc-gateway_out=logtostderr=true:C:\GoPath\src --go_out=plugins=grpc:C:\GoPath\src C:\GoPath\src\rsphonebook\server\servermodel\recordServer.proto

//swagger
//go:generate protoc -I=C:\GoPath\src -I=C:\GoPath\src\github.com\grpc-ecosystem\grpc-gateway\third_party\googleapis --swagger_out=logtostderr=true:C:\GoPath\src --go_out=plugins=grpc:C:\GoPath\src C:\GoPath\src\rsphonebook\server\servermodel\recordServer.proto


//bindata_assetfs.go
// скачиваем go get github.com/jteeuwen/go-bindata   go get github.com/elazarl/go-bindata-assetfs
// проинсталить, чтоб попал екзешник в bin:
// go install github.com/jteeuwen/go-bindata/go-bindata
// go install github.com/elazarl/go-bindata-assetfs/go-bindata-assetfs
// переходим в директорию C:\GoPath\src\rsphonebook\ui\swaggerui и выполняем команду go-bindata-assetfs -pkg=swaggerui static/...
//go:generate go-bindata-assetfs -pkg=swaggerui static/...

//Не работает

//go:generate protoc -I%GOPATH%/src/ -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis --grpc-gateway_out=logtostderr=true:D:/GoPath/src/gwProto/server/servermodel --go_out=plugins=grpc:D:/GoPath/src/gwProto/server/servermodel server/servermodel/contactServer.proto


