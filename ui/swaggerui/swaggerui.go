///go:generate sh -c "go-bindata-assetfs -pkg=swaggerui static/..."

package swaggerui

import (
	"net/http"
)

func GetFS() http.Handler {
	return http.FileServer(assetFS())
}
