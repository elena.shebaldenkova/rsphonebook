package server

import (
	tci_service_proto "rsphonebook/server/servermodel"
	swui "rsphonebook/ui/swaggerui"
	"github.com/gorilla/mux"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
	"net/http"
	"strings"
)

// Server represents a server for ui listeners
type Server struct {
	address string // address & port to start
}

// Creating new server
func NewServer(address string) *Server {
	s := &Server{address: address}
	return s
}

// Starting server
func (s Server) Start() error {
	r := mux.NewRouter()
	FS := swui.GetFS()

	// web
	//r.PathPrefix("/web/").
	//	Handler(http.StripPrefix("/web/", http.FileServer(http.Dir("./web/dist")))).
	//	Name("web")
	//r.HandleFunc("/", nil)
	r.PathPrefix("/swaggerui/").
		Handler(http.StripPrefix("/swaggerui/", FS)).
		Name("swaggerui")

	//api
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	mux := runtime.NewServeMux()
	dialOpts := []grpc.DialOption{grpc.WithInsecure()}
	err := tci_service_proto.RegisterPhoneBookHandlerFromEndpoint(ctx, mux, "localhost:10000", dialOpts)
	if err != nil {
		return err
	}

	r.PathPrefix("/api/").Handler(mux)

	// printing all routes
	printRoutes(r)
	// Bind to a port and pass our router in
	log.Fatal(http.ListenAndServe(s.address+":8080", r))

	return nil
}

// print all routes
func printRoutes(r *mux.Router) {

	err := r.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		t, err := route.GetPathTemplate()
		if err != nil {
			return err
		}
		log.Printf("GetPathTemplate:%v", t)
		qt, err := route.GetQueriesTemplates()
		//if err != nil {
		//	return err
		//}
		log.Printf("GetQueriesTemplates:%v", qt)
		// p will contain regular expression is compatible with regular expression in Perl, Python, and other languages.
		// for instance the regular expression for path '/articles/{id}' will be '^/articles/(?P<v0>[^/]+)$'
		p, err := route.GetPathRegexp()
		if err != nil {
			return err
		}
		log.Printf("GetPathRegexp:%v", p)
		// qr will contain a list of regular expressions with the same semantics as GetPathRegexp,
		// just applied to the Queries pairs instead, e.g., 'Queries("surname", "{surname}") will return
		// {"^surname=(?P<v0>.*)$}. Where each combined query pair will have an entry in the list.
		qr, err := route.GetQueriesRegexp()
		//if err != nil {
		//	return err
		//}
		log.Printf("GetQueriesRegexp:%v", qr)
		m, err := route.GetMethods()
		if err != nil {
			return err
		}
		log.Printf("GetMethods:%v", m)
		log.Println(strings.Join(m, ","), strings.Join(qt, ","), strings.Join(qr, ","), t, p)
		return nil
	})

	if err != nil {
		log.Fatalf("error walking routes:%v", err)
	}

}